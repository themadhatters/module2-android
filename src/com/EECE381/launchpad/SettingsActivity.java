package com.EECE381.launchpad;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import com.EECE381.launchpad.MainActivity;;

public class SettingsActivity extends PreferenceActivity {

	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.prefs);
	}
}
